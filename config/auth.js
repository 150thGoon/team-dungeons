// config/auth.js

module.exports = {
  'facebookAuth': {
    'clientID': '660521390789086', // your App ID
    'clientSecret': 'e9a5ed1f81e9458efd9de20bb13d5f57', // your App Secret
    'callbackURL': 'http://localhost:8080/auth/facebook/callback'
  },

  'twitterAuth': {
    'consumerKey': 'your-consumer-key-here',
    'consumerSecret': 'your-client-secret-here',
    'callbackURL': 'http://localhost:8080/auth/twitter/callback'
  },

  'googleAuth': {
    'clientID': 'your-secret-clientID-here',
    'clientSecret': 'your-client-secret-here',
    'callbackURL': 'http://localhost:8080/auth/google/callback'
  }
};